<!DOCTYPE html>
<#import "/spring.ftl" as spring/>
<html lang="ru">
<head>
    <#include "head.ftl">
</head>
<body>
<div class="container">
    <#include "header.ftl">
    <form class="form-signin" name="employee" action="" method="post" accept-charset="UTF-8">
        <h3 class="form-signin-heading">New employee</h3>
        <label for="firstname" class="sr-only">First name</label>
        <@spring.formInput "employee.firstname", "class=\"form-control\" placeholder=\"First name\" required autofocus", "text"/>
        <label for="lastname" class="sr-only">Last name</label>
        <@spring.formInput "employee.lastname", "class=\"form-control\" placeholder=\"Last name\" required", "text"/>
        <label for="surname" class="sr-only">Surname</label>
        <@spring.formInput "employee.surname", "class=\"form-control\" placeholder=\"Surname\" required", "text"/>
        <label for="email" class="sr-only">Email</label>
        <@spring.formInput "employee.email", "class=\"form-control\" placeholder=\"Email\" required", "email"/>
        <label for="phone" class="sr-only">Phone</label>
        <@spring.formInput "employee.phone", "class=\"form-control\" placeholder=\"Phone\" required", "text"/>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Save</button>
    </form>
</div>
</body>
</html>