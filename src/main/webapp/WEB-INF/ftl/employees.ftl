<!DOCTYPE html>
<#import "/spring.ftl" as spring/>
<html lang="ru">
<head>
    <#include "head.ftl">
</head>
<body>
<div class="container">

    <#include "header.ftl">

        <h3>Employees</h3>
    <div class="row marketing">
        <div class="col-lg-16">
            <#list employeeList as employee>
                <h4>${employee.firstname} ${employee.lastname} ${employee.surname}</h4>
                <p>phone: ${employee.phone} email: ${employee.email}</p>
            <#else>
                <p>We have no employees.</p>
            </#list>
        </div>

    </div>

</div>
</body>
</html>
