
<div class="header clearfix">
    <nav>
        <ul class="nav nav-pills pull-right">
            <li class="active"><a href=<@spring.url relativeUrl="/employees"/>>Employees</a></li>
            <li class="active"><a href=<@spring.url relativeUrl="/add_employee"/>>Add employee</a></li>
        </ul>
    </nav>
    <h3 class="text-muted">AT-Consulting</h3>
</div>
