package ru.enschin.attest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.enschin.attest.dao.DaoManager;
import ru.enschin.attest.model.Employee;

import java.util.List;

/**
 * Created by Andrey on 14.02.2017.
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    DaoManager daoManager;

    @Override
    @Transactional
    public List<Employee> getAllEmployees() {
        return daoManager.getAllEmployees();
    }

    @Override
    @Transactional
    public void addEmployee(Employee employee) {
        daoManager.addEmployee(employee);
    }
}
