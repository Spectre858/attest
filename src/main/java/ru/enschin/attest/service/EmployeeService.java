package ru.enschin.attest.service;

import ru.enschin.attest.model.Employee;

import java.util.List;

/**
 * Created by Andrey on 14.02.2017.
 */
public interface EmployeeService {
    List<Employee> getAllEmployees();
    void addEmployee(Employee employee);
}
