package ru.enschin.attest.dao;

import ru.enschin.attest.model.Employee;

import java.util.List;

/**
 * Created by Andrey on 13.02.2017.
 */
public interface DaoManager {
    List<Employee> getAllEmployees();
    void addEmployee(Employee employee);
}
