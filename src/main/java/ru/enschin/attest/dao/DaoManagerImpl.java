package ru.enschin.attest.dao;

import org.apache.log4j.Logger;
import org.hibernate.HibernateError;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.stereotype.Repository;
import ru.enschin.attest.model.Employee;

import java.util.List;

/**
 * Created by Andrey on 13.02.2017.
 */
@Repository
public class DaoManagerImpl implements DaoManager {
    private static final Logger LOGGER = Logger.getLogger(DaoManagerImpl.class);

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public List<Employee> getAllEmployees() {
        LOGGER.info("get all employees");
        return sessionFactory.getCurrentSession().createQuery("from Employee")
                .list();
    }

    @Override
    public void addEmployee(Employee employee) {
        LOGGER.info("add new employee");
        sessionFactory.getCurrentSession().save(employee);
    }
}
