package ru.enschin.attest.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.enschin.attest.model.Employee;
import ru.enschin.attest.service.EmployeeService;

import java.util.Map;

/**
 * Created by Andrey on 14.02.2017.
 */
@Controller
public class EmployeeController {
    private static final Logger LOGGER = Logger.getLogger(EmployeeController.class);
    @Autowired
    EmployeeService employeeService;

    @RequestMapping(value = "/employees")
    public String listEmployees(Map<String, Object> map) {
        LOGGER.info("do employees");

        map.put("employeeList", employeeService.getAllEmployees());

        return "employees";
    }

    @RequestMapping("/")
    public String home() {
        LOGGER.info("do home");
        return "redirect:/employees";
    }

    @RequestMapping(value = "/add_employee")
    public String addEmployee(Map<String, Object> map) {
        LOGGER.info("add employee");

        map.put("employee", new Employee());

        return "add_employee";
    }

    @RequestMapping(value = "/add_employee", method = RequestMethod.POST)
    public String doAddEmployee(@ModelAttribute("employee") Employee employee) {
        LOGGER.info("do add employee" + employee);

        employeeService.addEmployee(employee);

        return "redirect:/employees";
    }




}
